extern crate regex;

use regex::Regex;
use std::env;
use std::str::FromStr;

#[derive(Debug)]
struct Bumpy {
    major: u32,
    minor: u32,
    patch: u32,
    pre: String,
    build: String,
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let semver_regex = Regex::new(r"^(?P<major>0|[1-9]\d*)\.(?P<minor>0|[1-9]\d*)\.(?P<patch>0|[1-9]\d*)(?:-(?P<prere>(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+(?P<build>[0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$").unwrap();

    let captures = semver_regex.captures(&args[1]);

    if args.len() > 1 {
        match captures {
            Some(caps) => {
                let bumpy = Bumpy {
                    major: u32::from_str(caps.name("major").unwrap().as_str()).unwrap(),
                    minor: u32::from_str(caps.name("minor").unwrap().as_str()).unwrap(),
                    patch: u32::from_str(caps.name("patch").unwrap().as_str()).unwrap(),
                    pre: String::from("foo"),
                    build: String::from("foo"),
                };
                println!("{:?}", bumpy);
            }
            None => println!("No match found"),
        }
    } else {
        println!("Please provide a command-line argument.");
    }
}

// if let Some(caps) = re.captures(date) {
//     let year = caps.name("year").map_or("", |m| m.as_str());
//     let month = caps.name("month").map_or("", |m| m.as_str());
//     let day = caps.name("day").map_or("", |m| m.as_str());
//     println!("Year: {}, Month: {}, Day: {}", year, month, day);
// } else {
//     println!("Invalid date format");
// }
